package de.mindworker.p2pfx.simpleclientserverchatapp;

/**
 * Created by long on 20.06.15.
 */
public class OneComment {
    public enum Who {
        ME,
        BUDDY
    } ;

    public Who left;
    public String comment;

    public OneComment(Who left, String comment) {
        //super();
        this.left = left;
        this.comment = comment;
    }
}