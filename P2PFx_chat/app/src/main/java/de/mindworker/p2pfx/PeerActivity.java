package de.mindworker.p2pfx;

import android.app.Activity;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lakj.comspace.simpleclientserverchatapp.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.mindworker.p2pfx.simpleclientserverchatapp.ChatActivity;
import de.mindworker.p2pfx.simpleclientserverchatapp.OfferArrayAdapter;

public class PeerActivity extends Activity implements WifiP2pManager.PeerListListener, WifiP2pManager.ChannelListener {

    private final IntentFilter intentFilter = new IntentFilter();

    private boolean isWifiP2pEnabled;

    private Map<String, String> devicesMap = new HashMap<String, String>();

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private BroadcastReceiver mReceiver;

    public static String IPAddr;

    private OffersArrayAdapter adapter;
    private ListView lv;
    private OfferDbAdapter db;

    private int count;


    /** register the BroadcastReceiver with the intent values to be matched */
    @Override
    public void onResume() {
        super.onResume();
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
        registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peer);

        lv = (ListView) findViewById(R.id.offersList);

        adapter = new OffersArrayAdapter(this, R.layout.listitem_discuss);

        lv.setAdapter(adapter);
        lv.setDivider(null);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(PeerActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });

        db = new OfferDbAdapter(this);
        db.open();

        Cursor c = db.fetchAllOffer();
        while(c.moveToNext())
        {
            String mac = c.getString(c.getColumnIndex("mac"));
            String hasCurr = c.getString(c.getColumnIndex("have_currency"));
            String needCurr = c.getString(c.getColumnIndex("need_currency"));
            double hasAmount = c.getDouble(c.getColumnIndex("have_amount"));
            double needAmount = c.getDouble(c.getColumnIndex("need_amount"));
            String hash = c.getString(c.getColumnIndex("hash"));
            Offer offer = new Offer(hasCurr, needCurr,  hasAmount,  needAmount,  mac, hash);
            addOffer2List(offer);
        }


        count = 0;



        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);

        OfferServerAsyncTask ast = new OfferServerAsyncTask(this);
        ast.execute();

//        Button detectBtn = (Button) this.findViewById(R.id.detectPeerBtn);



//        detectBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
//                          Toast.makeText(getApplication(), "DETECT SUCCESS", Toast.LENGTH_LONG).show();
                Log.e("P2Pfx", "DiscoverPeerSuccess");
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.e("P2PFX", String.valueOf(reasonCode));
            }
        });
    }

    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }


    class RequestHandler implements Runnable {

        OfferDbAdapter dBHelper;


        @Override
        public void run() {

            Context context = getApplicationContext();
            dBHelper = new OfferDbAdapter(context);
            dBHelper.open();
            String host = PeerActivity.IPAddr;
            int port = 8888;
            Log.e("TAG",host);
            Socket socket = new Socket();
            try {
                /**
                 * Create a client socket with the host,
                 * port, and timeout information.
                 *
                 *
                 */


                socket.bind(null);
                socket.connect(new InetSocketAddress(host, port));
                Log.e("CONNECTED", String.valueOf(socket.isConnected()));

                OutputStream outputStream = socket.getOutputStream();
                ContentResolver cr = context.getContentResolver();
                ObjectOutputStream oos = new ObjectOutputStream(outputStream);
                oos.writeObject("FetchOffers");
                ObjectInputStream ios = new ObjectInputStream(socket.getInputStream());
                Offer offer = (Offer)ios.readObject();

                while (offer != null)
                {
                    dBHelper.createOffer(offer.getMac(),offer.getHasCurrency(), offer.getNeedCurrency(), offer.getHasAmount(), offer.getNeedAmount());
                    addOffer2List(offer);
                    offer = (Offer)ios.readObject();
                }
                ios.close();
                outputStream.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e)
            {
                e.printStackTrace();
            }

            finally {
                if (socket != null) {
                    if (socket.isConnected()) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            //catch logic
                        }
                    }
                }
            }
        }
    }

    static class OfferServerAsyncTask extends AsyncTask {

        private Context context;
        OfferDbAdapter dBHelper;

        public OfferServerAsyncTask(Context context) {
            this.context = context;
            dBHelper = new OfferDbAdapter(context);
            dBHelper.open();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (o != null) {
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                context.startActivity(intent);
            }
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {

                /**
                 * Create a server socket and wait for client connections. This
                 * call blocks until a connection is accepted from a client
                 */
                ServerSocket serverSocket = new ServerSocket(8888);
                Socket client = serverSocket.accept();


                InputStream inputstream = client.getInputStream();
                OutputStream outputStream = client.getOutputStream();
                ObjectInputStream ois = new ObjectInputStream(inputstream);
                ObjectOutputStream oos =  new ObjectOutputStream(outputStream);
                try {
                    if(ois.readObject().equals("FetchOffers"))
                    {

                        Cursor c = dBHelper.fetchAllOffer();
                        while (c.moveToNext())
                        {
                            String mac = c.getString(c.getColumnIndex("mac"));
                            String hasCurr = c.getString(c.getColumnIndex("have_currency"));
                            String needCurr = c.getString(c.getColumnIndex("need_currency"));
                            double hasAmount = c.getDouble(c.getColumnIndex("have_amount"));
                            double needAmount = c.getDouble(c.getColumnIndex("need_amount"));
                            String hash = c.getString(c.getColumnIndex("hash"));
                            Offer offer = new Offer(hasCurr, needCurr,  hasAmount,  needAmount,  mac, hash);
                            oos.writeObject(offer);
                        }
                    }
                    else
                    {
                        Log.e("LOW", ois.readObject().toString());
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                Log.e("ETT", inputstream.toString());

                serverSocket.close();
                return null;
            } catch (IOException e) {
                Log.e("Transfer", e.getMessage());
                return null;
            }
        }
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {
        Collection<WifiP2pDevice> devices = peers.getDeviceList();
        Iterator<WifiP2pDevice> it = devices.iterator();

        while(it.hasNext())
        {
            WifiP2pDevice dev = it.next();
            WifiP2pConfig config = new WifiP2pConfig();
            if((dev.deviceAddress.equals("7a:4b:87:b9:1a:4a") || dev.deviceAddress.equals("c0:ee:fb:26:b8:d1")) && count < 1)
            {
                count++;
                config.deviceAddress = dev.deviceAddress;
                config.groupOwnerIntent = 0;
                mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                            @Override
                            public void onConnectionInfoAvailable(WifiP2pInfo info) {

                                PeerActivity.IPAddr = info.groupOwnerAddress.getHostAddress();
                                RequestHandler requestHandler = new RequestHandler();
                                Thread t = new Thread(requestHandler);
                                t.start();

                            }
                        });
                    }

                    @Override
                    public void onFailure(int reason) {
                        Log.e("FAILURE REASON", String.valueOf(reason));
                    }
                });
            }

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_peer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onChannelDisconnected() {

    }

    public void addOffer2List(Offer of) {
        //final Offer entry = new Offer("USD", "HKD", 100, 1000, "lala", "  ");
        final Offer entry = of;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.add(entry);
            }
        });

    }

}
