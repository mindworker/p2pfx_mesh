package de.mindworker.p2pfx.simpleclientserverchatapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lakj.comspace.simpleclientserverchatapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by long on 20.06.15.
 */
public class DiscussArrayAdapter extends ArrayAdapter<OneComment> {

    private TextView countryName;
    private List<OneComment> countries = new ArrayList<OneComment>();
    private RelativeLayout wrapper;

    @Override
    public void add(OneComment object) {
        countries.add(object);
        super.add(object);
    }

    public DiscussArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public int getCount() {
        return this.countries.size();
    }

    public OneComment getItem(int index) {
        return this.countries.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        OneComment coment = getItem(position);

        LayoutInflater inflater;
        View row = convertView;
        if (row == null) {
            if (coment.left == OneComment.Who.ME) {
                inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.listitem_discuss, parent, false);
            }
            else {
                inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.listitem_discuss_right, parent, false);
            }
        }

        wrapper = (RelativeLayout) row.findViewById(R.id.odd_container);
        countryName = (TextView) row.findViewById(R.id.odd_text);
        countryName.setText(coment.comment);

        wrapper.setGravity(coment.left == OneComment.Who.ME ? Gravity.LEFT : Gravity.RIGHT);

        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}
