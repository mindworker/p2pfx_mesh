package de.mindworker.p2pfx;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lakj.comspace.simpleclientserverchatapp.R;

import java.util.ArrayList;
import java.util.List;

import de.mindworker.p2pfx.simpleclientserverchatapp.DiscussArrayAdapter;
import de.mindworker.p2pfx.simpleclientserverchatapp.OfferArrayAdapter;
import de.mindworker.p2pfx.simpleclientserverchatapp.OneComment;

public class MyOffersActivity extends Activity {

    private OfferArrayAdapter adapter;
    private ListView lv;
    private OfferDbAdapter db;
    private String MAC_STRING = "24:f5:aa:6f:5f:4f";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_offers);

        getActionBar().hide();

        lv = (ListView) findViewById(R.id.myOffersList);

        adapter = new OfferArrayAdapter(this, R.layout.listitem_discuss);
        addOffer2List(null); /// @todo remove

        lv.setAdapter(adapter);
        lv.setDivider(null);

        // Init DB
        db = new OfferDbAdapter(this);
        db.open();

        Cursor c = db.fetchOffersByUser(MAC_STRING);

        while (c.moveToNext()) {
            Offer of = new Offer(
                    c.getString(c.getColumnIndex(OfferDbAdapter.KEY_HAVECURR)),
                    c.getString(c.getColumnIndex(OfferDbAdapter.KEY_NEEDCURR)),
                    c.getDouble(c.getColumnIndex(OfferDbAdapter.KEY_HAVEAMOUNT)),
                    c.getDouble(c.getColumnIndex(OfferDbAdapter.KEY_NEEDAMOUNT)),
                    c.getString(c.getColumnIndex(OfferDbAdapter.KEY_MAC)),
                    null); /// @note really like this?
            addOffer2List(of);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addOffer2List(Offer of) {
        //final Offer entry = new Offer("USD", "HKD", 100, 1000, "lala", "  ");
        final Offer entry = of;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.add(entry);
            }
        });

    }
}
