package de.mindworker.p2pfx.simpleclientserverchatapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.lakj.comspace.simpleclientserverchatapp.R;

import de.mindworker.p2pfx.AddOfferActivity;
import de.mindworker.p2pfx.MyOffersActivity;
import de.mindworker.p2pfx.PeerActivity;


/**
 * Created by igorwos on 21/6/15.
 */
public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendToPeers(View view)
    {
        Intent intent = new Intent(MenuActivity.this, PeerActivity.class);
        startActivity(intent);
    }

    public void sendToAddOffer(View view)
    {
        Intent intent = new Intent(MenuActivity.this, AddOfferActivity.class);
        startActivity(intent);
    }

    public void sendToMyOffers(View view)
    {
        Intent intent = new Intent(MenuActivity.this, MyOffersActivity.class);
        startActivity(intent);
    }

    public void sendToMessages(View view)
    {
        Intent intent = new Intent(MenuActivity.this, ChatActivity.class);
        startActivity(intent);
    }

    public void sendToSplash(View view)
    {
        Intent intent = new Intent(MenuActivity.this, SplashActivity.class);
        startActivity(intent);
    }
//    public void myOffers(View view)
//    {
//        Intent intent = new Intent(MenuActivity.this, Offers.class);
//        startActivity(intent);
//    }

}

