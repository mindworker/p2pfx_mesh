package de.mindworker.p2pfx;

import java.io.Serializable;

/**
 * Created by moin and wos on 6/20/15.
 */
public class Offer implements Serializable {

    private String hasCurrency;
    private String needCurrency;
    private double hasAmount;
    private double needAmount;
    private String mac;
    private String hash;

    public String getHasCurrency() {
        return hasCurrency;
    }

    public void setHasCurrency(String hasCurrency) {
        this.hasCurrency = hasCurrency;
    }

    public String getNeedCurrency() {
        return needCurrency;
    }

    public void setNeedCurrency(String needCurrency) {
        this.needCurrency = needCurrency;
    }

    public double getHasAmount() {
        return hasAmount;
    }

    public void setHasAmount(double hasAmount) {
        this.hasAmount = hasAmount;
    }

    public double getNeedAmount() {
        return needAmount;
    }

    public void setNeedAmount(double needAmount) {
        this.needAmount = needAmount;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Offer(String hasCurrency, String needCurrency, double hasAmount, double needAmount, String mac, String hash) {
        this.hasCurrency = hasCurrency;
        this.needCurrency = needCurrency;
        this.hasAmount = hasAmount;
        this.needAmount = needAmount;
        this.mac = mac;
        this.hash = hash;
    }

    public String hashOffer()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(hasCurrency);
        sb.append(needCurrency);
        sb.append(hasAmount);
        sb.append(needAmount);
        sb.append(mac);
        String hash = sb.toString();

        this.setHash(hash);
        return  hash;
    }

}
