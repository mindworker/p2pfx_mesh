package de.mindworker.p2pfx;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakj.comspace.simpleclientserverchatapp.R;

import java.util.ArrayList;
import java.util.List;

import de.mindworker.p2pfx.Offer;

public class OffersArrayAdapter extends ArrayAdapter<Offer> {

    private TextView haveCurr;
    private TextView needCurr;
    private TextView haveAmount;
    private TextView needAmount;
    private List<Offer> offers = new ArrayList<Offer>();
    private LinearLayout wrapper;

    @Override
    public void add(Offer object) {
        offers.add(object);
        super.add(object);
    }

    public OffersArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public int getCount() {
        return this.offers.size();
    }

    public Offer getItem(int index) {
        return this.offers.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Offer coment = getItem(position);

        View row = convertView;

        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.offers_row, parent, false);

        if(coment == null) {
            System.out.println("offer is null!");
            return row;
        }

        wrapper = (LinearLayout) row.findViewById(R.id.offers_list_container);

        needCurr = (TextView) row.findViewById(R.id.needCurr);
        needCurr.setText(coment.getNeedCurrency());

        needAmount = (TextView) row.findViewById(R.id.needAmount);
        needAmount.setText(String.valueOf(coment.getNeedAmount()));

        haveCurr = (TextView) row.findViewById(R.id.haveCurr);
        haveCurr.setText(coment.getHasCurrency());

        haveAmount = (TextView) row.findViewById(R.id.haveAmount);
        haveAmount.setText(String.valueOf(coment.getHasAmount()));

        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}