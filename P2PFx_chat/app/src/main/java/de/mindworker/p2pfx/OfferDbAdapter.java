/*
 * Copyright (C) 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package de.mindworker.p2pfx;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Simple notes database access helper class. Defines the basic CRUD operations
 * for the notepad example, and gives the ability to list all notes as well as
 * retrieve or modify a specific note.
 *
 * This has been improved from the first version of this tutorial through the
 * addition of better error handling and also using returning a Cursor instead
 * of using a collection of inner classes (which is less scalable and not
 * recommended).
 */
public class OfferDbAdapter {

    public static final String KEY_HAVECURR = "have_currency";
    public static final String KEY_HAVEAMOUNT = "have_amount";
    public static final String KEY_NEEDCURR = "need_currency";
    public static final String KEY_NEEDAMOUNT = "need_amount";
    public static final String KEY_ROWID = "_id";
    public static final String KEY_MAC = "mac";
    public static final String KEY_HASH = "hash";

    private static final String TAG = "OfferDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    /**
     * Database creation sql statement
     */
    private static final String DATABASE_CREATE =
            "create table offers (_id integer primary key autoincrement, "
                    + "mac text not null, have_currency text not null, need_currency text not null, have_amount real not null, need_amount real not null, hash text );";

    private static final String DATABASE_NAME = "P2Pfx";
    private static final String DATABASE_TABLE = "offers";
    private static final int DATABASE_VERSION = 3;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS offer");
            onCreate(db);
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param ctx the Context within which to work
     */
    public OfferDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public OfferDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }


    public long createOffer(String mac, String haveCurrency, String needCurrency, double haveAmount, double needAmount) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_HAVECURR, haveCurrency);
        initialValues.put(KEY_HAVEAMOUNT, haveAmount);
        initialValues.put(KEY_NEEDCURR, needCurrency);
        initialValues.put(KEY_NEEDAMOUNT, needAmount);
        initialValues.put(KEY_MAC, mac);

        Offer off = new Offer(haveCurrency, needCurrency, haveAmount, needAmount, mac, "hash");
        String hash = off.hashOffer();
        initialValues.put(KEY_HASH, hash);

        Cursor c = this.fetchOfferByHash(hash);
        if(c.getCount() == 0)
            return mDb.insert(DATABASE_TABLE, null, initialValues);
        else
            return -1;
    }

    public boolean deleteOffer(long rowId) {

        return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

//    /**
//     * Delete the note with the given userId
//     *
//     * @param targetId id of note to delete
//     * @return true if deleted, false otherwise
//     */
//    public boolean deleteUser(int targetId) {
//
//        return mDb.delete(DATABASE_TABLE, KEY_TARGETID + "=" + targetId, null) > 0;
//    }

    /**
     * Return a Cursor over the list of all notes in the database
     *
     * @return Cursor over all notes
     */
    public Cursor fetchAllOffer() {

        return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_HAVEAMOUNT, KEY_HAVECURR,
                KEY_NEEDAMOUNT, KEY_NEEDCURR, KEY_MAC, KEY_HASH}, null, null, null, null, null, null);
    }

    /**
     * Return a Cursor positioned at the note that matches the given rowId
     *
     * @param rowId id of note to retrieve
     * @return Cursor positioned to matching note, if found
     * @throws SQLException if note could not be found/retrieved
     */
    public Cursor fetchOffer(long rowId) throws SQLException {

        Cursor mCursor =

                mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                                KEY_HAVEAMOUNT, KEY_HAVECURR,
                                KEY_NEEDAMOUNT, KEY_NEEDCURR, KEY_MAC, KEY_HASH}, KEY_ROWID + "=" + rowId, null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    public Cursor fetchOffersByUser(String mac) throws SQLException {

        Cursor mCursor =

                mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_MAC, KEY_HAVEAMOUNT,
                                KEY_HAVECURR, KEY_NEEDAMOUNT, KEY_NEEDCURR}, KEY_MAC + "='" + mac + "'", null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    public Cursor fetchOfferByHash(String hash) throws SQLException {

        Cursor mCursor =

                mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_MAC, KEY_HAVEAMOUNT,
                                KEY_HAVECURR, KEY_NEEDAMOUNT, KEY_NEEDCURR}, KEY_HASH + "='" + hash+"'", null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    public boolean updateOffer(long rowId, String mac, String haveCurrency, String needCurrency, int haveAmount, int needAmount) {
        ContentValues args = new ContentValues();
        args.put(KEY_HAVECURR, haveCurrency);
        args.put(KEY_HAVEAMOUNT, haveAmount);
        args.put(KEY_NEEDCURR, haveAmount);
        args.put(KEY_NEEDAMOUNT, haveAmount);
        args.put(KEY_MAC, mac);

        Offer off = new Offer(haveCurrency, needCurrency, haveAmount, needAmount, mac, "hash");
        args.put(KEY_HASH, off.hashOffer());


        return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

}