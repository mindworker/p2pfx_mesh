package de.mindworker.p2pfx.simpleclientserverchatapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakj.comspace.simpleclientserverchatapp.R;

import java.util.ArrayList;
import java.util.List;

import de.mindworker.p2pfx.Offer;

/**
 * Created by long on 21.06.15.
 */

public class OfferArrayAdapter extends ArrayAdapter<Offer> {

    private TextView offerCurr;
    private TextView forCurr;
    private List<Offer> countries = new ArrayList<Offer>();
    private LinearLayout wrapper;

    @Override
    public void add(Offer object) {
        countries.add(object);
        super.add(object);
    }

    public OfferArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public int getCount() {
        return this.countries.size();
    }

    public Offer getItem(int index) {
        return this.countries.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Offer coment = getItem(position);

        View row = convertView;

        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.offer_row, parent, false);

        if(coment == null) {
            System.out.println("offer is null!");
            return row;
        }

        wrapper = (LinearLayout) row.findViewById(R.id.offer_list_container);

        offerCurr = (TextView) row.findViewById(R.id.offerCurr);
        offerCurr.setText(coment.getHasCurrency() + " " + coment.getHasAmount());

        forCurr = (TextView) row.findViewById(R.id.forCurr);
        forCurr.setText(coment.getNeedCurrency() + " " + coment.getNeedAmount());

        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}
