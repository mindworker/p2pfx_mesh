package de.mindworker.p2pfx;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.lakj.comspace.simpleclientserverchatapp.R;

public class OfferActivity extends Activity {

    private OffersArrayAdapter adapter;
    private ListView lv;
    private OfferDbAdapter db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        lv = (ListView) findViewById(R.id.offersList);

        adapter = new OffersArrayAdapter(this, R.layout.activity_offer);
        addOffer2List(null); /// @todo remove

        lv.setAdapter(adapter);
        lv.setDivider(null);

        // Init DB
        db = new OfferDbAdapter(this);
        db.open();

        Cursor c = db.fetchAllOffer();

        while (c.moveToNext())
        {
            Offer offer = new Offer( c.getString(c.getColumnIndex("have_currency")), c.getString(c.getColumnIndex("need_currency")), c.getDouble(c.getColumnIndex("have_amount")), c.getDouble(c.getColumnIndex("need_amount")),c.getString(c.getColumnIndex("mac")), c.getString(c.getColumnIndex("hash")));
            addOffer2List(offer);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addOffer2List(Offer of) {
        //final Offer entry = new Offer("USD", "HKD", 100, 1000, "lala", "  ");
        final Offer entry = of;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.add(entry);
            }
        });

    }
}
