package de.mindworker.p2pfx.simpleclientserverchatapp;

/**
 * Courtesy: http://lakjeewa.blogspot.hk/2015/01/android-client-server-chat-application.html
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.lakj.comspace.simpleclientserverchatapp.R;

public class ChatActivity extends Activity {

	private EditText textField;
	private Button button;
	private TextView textView;

	// For test purposes only
//	private volatile String CHAT_PARTNER_IP = "175.159.187.165"; // Emulator
//	private volatile String CHAT_PARTNER_IP = "175.159.162.82";  // Nexus
//  private volatile String CHAT_PARTNER_IP = "175.159.187.115"; // Samsung
//  private volatile String CHAT_PARTNER_IP = "175.159.187.133"; // Mi

	private String PARTNER_DISPLAY_NAME = "Steve";
	private String CONNECTED;

	private Socket clientReceive;
	private Socket clientTransmit;
	private PrintWriter printwriter;
	private BufferedReader bufferedReader;
	private ServerSocket serverSocket;

	static boolean active = false;
	private NotificationManager mNotifyMgr;
	private DiscussArrayAdapter adapter;
	private ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setTitle("Chat");
		CONNECTED = getResources().getString(R.string.connT);
		getActionBar().hide();

		lv = (ListView) findViewById(R.id.listView1);
		adapter = new DiscussArrayAdapter(this, R.layout.listitem_discuss);

		addChatLine(OneComment.Who.ME, "[Me connected]");

		lv.setAdapter(adapter);
		lv.setDivider(null);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		textField = (EditText) findViewById(R.id.editText1);
		button = (Button) findViewById(R.id.button1);
		textView = (TextView) findViewById(R.id.textView1);

		// Gets an instance of the NotificationManager service
		mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		// Create Socketserver to receive messages
		try {
			serverSocket = new ServerSocket(4444);
		} catch (IOException e) {
			e.printStackTrace();
		}

		ChatOperator chatOperator = new ChatOperator();
		chatOperator.execute();

		TextView meConnectedTextView = (TextView) findViewById(R.id.titleTextView);
		meConnectedTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				textField.setText(""); // Clear the chat box
				addChatLine(OneComment.Who.BUDDY, CONNECTED);
			}
		});
	}

    public void addChatLine(OneComment.Who is_me, String message) {
        final OneComment entry = new OneComment(is_me, message);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.add(entry);
            }
        });
    }

	private class ChatOperator extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
			try {

//				// This thread tries to connect to the chat partner while allowing other chatpartners to connect
//				Thread thread = new Thread() {
//					@Override
//					public void run() {
//						while(clientTransmit == null) {
//							try {
//								System.out.println("Trying to connect to transmit socket.");
//								clientTransmit = new Socket(CHAT_PARTNER_IP, 4444); // Creating the server socket
//							} catch (java.net.ConnectException e) {
//								try {
//									System.out.println("Failed. Trying to connect again...");
//									this.sleep(500);
//								} catch (InterruptedException ex) {
//									ex.printStackTrace();
//								}
//							} catch (UnknownHostException e) {
//								e.printStackTrace();
//							} catch (IOException e) {
//								e.printStackTrace();
//							}
//						}
//					}
//				};
//				thread.start();

				//clientReceive = serverSocket.accept();
				clientReceive = new Socket();
				clientTransmit = new Socket();

				System.out.println("Connect Receive Socket.");

				if (clientReceive != null) {

					InputStreamReader inputStreamReader = new InputStreamReader(clientReceive.getInputStream());
					bufferedReader = new BufferedReader(inputStreamReader);

					//thread.join();		// wait until someone has connected
                    addChatLine(OneComment.Who.BUDDY, "[" + PARTNER_DISPLAY_NAME + " connected]");

					printwriter = new PrintWriter(clientTransmit.getOutputStream(), true);

				} else {
					System.out.println("Server has not bean started on port 4444.");
				}
			} catch (Exception e) {
				System.out.println("Faild to connect server");
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * Following method is executed at the end of doInBackground method.
		 */
		@Override
		protected void onPostExecute(Void result) {
			button.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					final Sender messageSender = new Sender(); // Initialize chat sender AsyncTask.
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						messageSender.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					} else {
						messageSender.execute();
					}
				}
			});

			Receiver receiver = new Receiver(); // Initialize chat receiver AsyncTask.
			receiver.execute();

		}

	}

	/**
	 * This AsyncTask continuously reads the input buffer and show the chat
	 * message if a message is availble.
	 */
	private class Receiver extends AsyncTask<Void, Void, Void> {

		private String message;

		@Override
		protected Void doInBackground(Void... params) {
			while (true) {

				if (bufferedReader != null) {

					try {
						if (bufferedReader.ready()) {
							message = bufferedReader.readLine();
							publishProgress(null);
						}
					} catch (UnknownHostException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				try {
					Thread.sleep(500);
				} catch (InterruptedException ie) {
				}
			}
		}

		@Override
		protected void onProgressUpdate(Void... values) {

			if (!active) {  // send out notification
				Intent resultIntent = new Intent(getApplicationContext(), ChatActivity.class);

				PendingIntent resultPendingIntent =
						PendingIntent.getActivity(
								getApplicationContext(),
								0,
								resultIntent,
								PendingIntent.FLAG_UPDATE_CURRENT
						);

				NotificationCompat.Builder mBuilder =
						new NotificationCompat.Builder(getApplicationContext())
								.setSmallIcon(R.drawable.ic_message_black_24dp)
								.setContentTitle("New Message from PlainExchange!")
								.setContentText(PARTNER_DISPLAY_NAME + ": " + message + "\n")
								.setContentIntent(resultPendingIntent);

				// Sets an ID for the notification
				int mNotificationId = 001;


				// Builds the notification and issues it.
				mNotifyMgr.notify(mNotificationId, mBuilder.build());
			}

			//textView.append(PARTNER_DISPLAY_NAME + ": " + message + "\n");
			addChatLine(OneComment.Who.BUDDY, message);
		}

	}

	/**
	 * This AsyncTask sends the chat message through the output stream.
	 */
	private class Sender extends AsyncTask<Void, Void, Void> {

		private String message;

		@Override
		protected Void doInBackground(Void... params) {
			message = textField.getText().toString();

			if (printwriter != null) {
				printwriter.write(message + "\n");
				printwriter.flush();
			}
			else {
				message = textField.getText().toString() + " [N/A]";
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			textField.setText(""); // Clear the chat box
			//textView.append("Me: " + message + "\n");
			if (clientTransmit != null) {
				message = CONNECTED;
			}
			addChatLine(OneComment.Who.ME, message);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onStart() {
		super.onStart();
		active = true;
		mNotifyMgr.cancelAll();
	}

	@Override
	public void onStop() {
		super.onStop();
		active = false;
	}

}