package de.mindworker.p2pfx;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.lakj.comspace.simpleclientserverchatapp.R;

public class AddOfferActivity extends Activity {

    public Context con;

    private OfferDbAdapter db;
    private String MAC_STRING = "24:f5:aa:6f:5f:4f";
    private EditText haveEditText;
    private EditText needEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_offer);

        getActionBar().hide();
        con = this;

        // Init Views
        haveEditText = (EditText) findViewById(R.id.currHave);
        needEditText = (EditText) findViewById(R.id.currNeed);

        // Currencie Spinners
        final Spinner spinnerHave = (Spinner) findViewById(R.id.spinnerCurrHave);
        final Spinner spinnerNeed = (Spinner) findViewById(R.id.spinnerCurrNeed);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.currencies, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinnerHave.setAdapter(adapter);
        spinnerNeed.setAdapter(adapter);

        // Init DB
        db = new OfferDbAdapter(this);
        db.open();

        ((Button) findViewById(R.id.addOfferbutton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                db.createOffer(
                        MAC_STRING,
                        spinnerHave.getSelectedItem().toString(),
                        spinnerNeed.getSelectedItem().toString(),
                        Float.parseFloat(haveEditText.getText().toString()),
                        Float.parseFloat(needEditText.getText().toString()));

                System.out.println(db.fetchAllOffer().getCount());

                Intent intent = new Intent(AddOfferActivity.this, MyOffersActivity.class);
                startActivity(intent);

                Toast.makeText(con, "Offer added.", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_offer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
